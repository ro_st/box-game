import React, { Component } from 'react';
import MovableBox from './MovableBox';
import './ContentBox.css';

class ContentBox extends Component {

    constructor() {
        super();
        this.state = {
            displayCordinates: true,
        };
    }

    toggleCordsDisplay() {
        this.setState(prevState => ({
                displayCordinates: !prevState.displayCordinates
        }));
    }

    render() {
        
        const parentClassName = "content-box";

        return (
            <div
                className={parentClassName}
                onClick={this.toggleCordsDisplay.bind(this)}>
                <MovableBox
                    displayCordinates={this.state.displayCordinates}
                    parentSelector={parentClassName} />
            </div>
        );
    }
}

export default ContentBox;