import React, { Component } from 'react';
import keyCodes from './config/keyCodes';
import './MovableBox.css';

class MovableBox extends Component {

    constructor() {
        super();
        
        this.state = {
            KEY: keyCodes,
            TICK: {
                incr: { x: 10, y: 10 },
                decr: { x: 10, y: 10 },
            },
            RADIX: 10,
            screen: {
                width: 0,
                height: 0
            },
            cordinates: {
                x: 0,
                y: 0
            },
        };

    }

    componentDidMount() {
        window.addEventListener('keydown', this.handleKeys.bind(this));
        // window.addEventListener('resize', this.setScreen.bind(this, false));

        this.setScreen(true);
    }

    componentWillUnmount() {
        // window.removeEventListener('keyup', this.handleKeys);
        window.removeEventListener('keydown', this.handleKeys);
    }

    // returns cordinates prepared for display
    getDisplayCordinates(cords) {
        const { x, y } = cords;

        // 0,0 (x,y) is in the top left corner but CSS will set it to centered
        // recalculate x to actually display it as x, otherwise we would spit out 
        return {
            x: parseInt(x + this.state.screen.width / 2, this.state.RADIX),
            y: parseInt(y + this.state.screen.height / 2, this.state.RADIX),
        };
    }

    // method to update screen state with
    setScreen(init=false) {
        let { parentSelector } = this.props;
        const parent = document.querySelector(`.${parentSelector}`);

        const { width, height } = parent.getBoundingClientRect();

        // initial values
        const initial = {
            x: parseInt("-" + width / 2, this.state.RADIX),
            y: parseInt("-" + height / 2, this.state.RADIX),
        };

        
        // if init and screen is not set, i.e screen uses default value 0
        if (init && this.state.screen.width === 0) {
            
            this.setState(prevState => ({
                ...prevState,
                screen: {
                    width,
                    height,
                },
                cordinates: {
                    x: initial.x,
                    y: initial.y,
                },
            }));

        } else {
            
            this.setState(prevState => ({
                ...prevState,
                screen: {
                    width,
                    height,
                },
            }));

        }
    }

    // update cordinates with passed config
    updateCordinates(updateConfig) {
        const { axis, mode, value } = updateConfig;
        const cords = this.state.cordinates;
        const screen = this.state.screen;
        
        // update checks
        const update = {
            is: {
                manualWrite: mode === '_',
                incr: mode === '+',
                decr: mode === '-',
                x: axis === 'x',
                y: axis === 'y',
            },
        };

        // tests to return true if a limit have been reached
        const limitReached = {
            x: {
                max: cords.x > screen.width / 2 - this.state.TICK.incr.x,
                min: cords.x === parseInt("-" + screen.width / 2, this.state.RADIX) || cords.x < parseInt("-" + screen.width / 2, this.state.RADIX),
            },
            y: {
                max: cords.y === screen.height / 2 || cords.y > screen.height / 2,
                min: cords.y === 0 - screen.height / 2 || cords.y < 0 - screen.height / 2,
            },
        };

        console.log(cords.y, screen.height, screen.height / 2)

        // abort cordinate update if it's limit have been reached
        // TODO: set cordinates state to it's max or min value depending on update if the limit have been reached
        if (update.is.incr && update.is.x && limitReached.x.max) { return; }
        if (update.is.decr && update.is.x && limitReached.x.min) { return; }
        if (update.is.incr && update.is.y && limitReached.y.max) { return; }
        if (update.is.decr && update.is.y && limitReached.y.min) { return; }

        // if we simply want to update state to specific cordinates
        if (update.is.manualWrite) {
            this.setState(prevState => ({
                ...prevState,
                cordinates: {
                    ...prevState.cordinates,
                    [axis]: value,
                }
            }));
            
            return;
        }
        
        this.setState(prevState => {

            // a fn() to return cordinates to state
            //  handles which axis to target
            const returnCordinates = function(newCordinate) {
                if (update.is.x) {
                    return {
                        cordinates: {
                            ...prevState.cordinates,
                            [axis]: newCordinate,
                        },
                    };
                } else if (update.is.y) {
                    return {
                        cordinates: {
                            ...prevState.cordinates,
                            [axis]: newCordinate,
                        },
                    };
                }
            };

            // check if we want to increase or decrease state
            if (update.is.incr) {
                return returnCordinates(prevState.cordinates[axis] + value);
            } else {
                return returnCordinates(prevState.cordinates[axis] - value);
            }
        });

    }

    handleKeys(e) {
        // config objects to pass when updating cordinates
        const boxConfig = {
            x: {
                incr: {
                    axis: 'x',
                    mode: '+',
                    value: this.state.TICK.incr.x,
                },
                decr: {
                    axis: 'x',
                    mode: '-',
                    value: this.state.TICK.incr.y,
                }
            },
            y: {
                incr: {
                    axis: 'y',
                    mode: '+',
                    value: this.state.TICK.incr.y,
                },
                decr: {
                    axis: 'y',
                    mode: '-',
                    value: this.state.TICK.decr.y,
                },
            },
        };

        // return passed key if exists in state
        const emitValidKeyCode = (keyCode) => {
            for (let code in this.state.KEY) {
                if (this.state.KEY[code] === keyCode) {
                    return this.state.KEY[code];
                }
            }
        };

        // check keyCode and then run cordinate update with the right config
        switch(emitValidKeyCode(e.keyCode)) {
            case this.state.KEY.UP:
                this.updateCordinates(boxConfig.y.decr);
                break;
            case this.state.KEY.DOWN:
                this.updateCordinates(boxConfig.y.incr);
                break;
            case this.state.KEY.RIGHT:
                this.updateCordinates(boxConfig.x.incr);
                break;
            case this.state.KEY.LEFT:
                this.updateCordinates(boxConfig.x.decr);
                break;
            case this.state.KEY.ENTER:
                this.randomize();
                break;
            case this.state.KEY.HOME:
                this.resetCordinates();
                break;
            default:
        }

    }

    // set specific cordinates
    setCordinates(x=0, y=0) {
        // 0,0 (x,y) is in the top left corner but CSS will set it to centered
        // recalculate so passed arg x is correct when updating cordinates
        const xChange = parseInt("-" + this.state.screen.width / 2, this.state.RADIX);
        const yChange = parseInt(x + this.state.screen.width / 2, this.state.RADIX)
        x = x + xChange;
        y = y + yChange;

        this.updateCordinates({ axis: 'x', mode: '_', value: x });
        this.updateCordinates({ axis: 'y', mode: '_', value: y });
    }

    // set random cordinates
    randomize() {
        const { width, height } = this.state.screen;
        const x = Math.round(Math.random() * width);
        const y = Math.round(Math.random() * height);

        this.setCordinates(x, y);
    }

    // resets cordinates to origo
    resetCordinates() {
        this.setCordinates();
    }

    render() {
        const cordinates = this.state.cordinates;
        
        const style = {
            transform: `translate(${cordinates.x}px, ${cordinates.y}px)`,
        };
        
        let cordinatesDisplay = null;

        if (this.props.displayCordinates) {
            const { x, y } = this.getDisplayCordinates(cordinates);
            cordinatesDisplay = <div className="cordinates-display">({x},{y})</div>
        }

        return (
            <div
                className="movable-box"
                onClick={this.resetCordinates.bind(this)}
                style={style}>{cordinatesDisplay}</div>
        );
    }

}

export default MovableBox;