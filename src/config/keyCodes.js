const keys ={
    DOWN: 40,
    RIGHT: 39,
    UP: 38,
    LEFT: 37,
    HOME: 36,
    END: 35,
    ENTER: 13,
    SHIFT: 16,
};

export default keys;